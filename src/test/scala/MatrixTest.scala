import org.scalatest._
import com.ml.linearalgebra._

class MatrixOps extends FlatSpec with Matchers {

  //Initialisation
  val r = 4
  val c = 4
  val t1 = Array(2, 3, 4, 5)
  val t2 = Array(1, 2, 3, 4)
  val m1 = Matrix(2, 2, t1)
  val m2 = Matrix(2, 2, t2)

  //Expected output
  val expAdd = Array(3, 5, 7, 9)
  val expSub = Array(1, 1, 1, 1)
  val expMult = Array(11, 16, 19, 28)
  "This matrix" should "add two matrices" in {
    val output = m1 + m2
    val elems = output.data.flatten
    elems.deep == expAdd.deep should be(true)
  }

  "This matrix" should "subtract two matrices" in {
    val output = m1 - m2
    val elems = output.data.flatten
    elems.deep == expSub.deep should be(true)
  }

  "This matrix" should "multiply two matrices" in {
    val output = m1 * m2
    val elems = output.data.flatten
    elems.deep == expMult.deep should be(true)
  }
}



