package com.ml.linearalgebra

object Matrix {

  def arrNext[T](idx: Int, arr: Array[T]) = {
    if (idx < arr.length && idx >= 0)
      (arr(idx), idx + 1)
    else (arr(0), -1)
  }

  def constructMatrix[T](r: Int, c: Int, arr: Array[T])(implicit nn: Numeric[T], mm: ClassManifest[T]): Matrix[T] = {
    val tArr = Array.ofDim[T](r, c)
    var next = 0

    for (
      i <- 0 until r;
      j <- 0 until c
    ) {
      val (d, n) = arrNext(next, arr)
      if (n != -1) {
        tArr(i)(j) = d
        next = n
      }
    }

    new Matrix(tArr)
  }

  def apply[T](r: Int, c: Int, m: Array[T])(implicit nm: Numeric[T], cm: ClassManifest[T]) = {
    constructMatrix(r, c, m)
  }
}

class Matrix[T: Numeric: ClassManifest](val data: Array[Array[T]]) {
  val num = implicitly[Numeric[T]]
  val r = data.size
  val c = data(0).size

  def pieceWise(f: (T, T) => T, that: Matrix[T]): Matrix[T] = {
    val tArr = Array.ofDim[T](r, c)
    for (
      i <- 0 until r;
      j <- 0 until c
    ) {
      tArr(i)(j) = f(data(i)(j), that.data(i)(j))
    }
    new Matrix(tArr)
  }

  def +(that: Matrix[T]) = pieceWise(num.plus(_, _), that)
  def -(that: Matrix[T]) = pieceWise(num.minus(_, _), that)
  def *(that: Matrix[T]) = {
    val arr = Array.ofDim[T](r)
    val temp = Matrix[T](r, that.c, arr)
    for (
      i <- 0 until r;
      j <- 0 until c;
      k <- 0 until that.c
    ) {
      temp.data(i)(k) = num.plus(temp.data(i)(k), num.times(data(i)(j), that.data(j)(k)))
    }
    temp
  }

  override def toString = {
    var str = ""
    for (i <- 0 to r - 1) {
      for (j <- 0 to c - 1) {
        str += data(i)(j) + "\t"
      }
      str += "\n"
    }
    str
  }

}

